import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlotContainerComponent } from './plot-container.component';

describe('PlotContainerComponent', () => {
  let component: PlotContainerComponent;
  let fixture: ComponentFixture<PlotContainerComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PlotContainerComponent]
    });
    fixture = TestBed.createComponent(PlotContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
