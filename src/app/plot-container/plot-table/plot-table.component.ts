import { Component, Input } from '@angular/core';
import { Covid, CovidModel } from 'src/app/covid.model';

@Component({
  selector: 'app-plot-table',
  templateUrl: './plot-table.component.html',
  styleUrls: ['./plot-table.component.css']
})
export class PlotTableComponent {
  // @Input() data: CovidModel[] = [];
  @Input() data: Covid[] = [];
  headers = Object.keys(new CovidModel());

  ngOnInit() {
    console.log('Data:', this.data);
  }

  
  printData(){
    console.log(this.data)
  }

}
