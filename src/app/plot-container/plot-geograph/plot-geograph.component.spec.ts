import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlotGeographComponent } from './plot-geograph.component';

describe('PlotGeographComponent', () => {
  let component: PlotGeographComponent;
  let fixture: ComponentFixture<PlotGeographComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PlotGeographComponent]
    });
    fixture = TestBed.createComponent(PlotGeographComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
