import { Component, Input, SimpleChanges } from '@angular/core';
import { Chart } from 'chart.js';
import { Covid } from 'src/app/covid.model';

@Component({
  selector: 'app-plot-geograph',
  templateUrl: './plot-geograph.component.html',
  styleUrls: ['./plot-geograph.component.css']
})
export class PlotGeographComponent {
  @Input() dataBars2: Covid[] = [];
  public chart: any;

  private countries: string[] = [];
  private totalCases: number[] = [];


  constructor() {
    this.countries = [];
    this.totalCases = [];

  }

  ngOnInit() {
    
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.dataBars2.length > 0) this.createChart();
  }

  createChart() {
    this.countries = this.getCountries();
    this.totalCases = this.getCases();

  
    if (this.chart) this.chart.destroy();
  
    this.chart = new Chart("Bars2Chart", {
      type: 'bar',
      data: {
        labels: this.countries,
        datasets: [
          {
            label: "Total Cases",
            data: this.totalCases, 
            backgroundColor: [
              'rgb(255, 99, 132)',
              'rgb(255, 159, 64)',
              'rgb(255, 205, 86)',
              'rgb(75, 192, 192)',
              'rgb(54, 162, 235)',
              'rgb(153, 102, 255)',
              'rgb(201, 203, 207)',
              'rgb(255, 99, 132)',
              'rgb(255, 159, 64)',
              'rgb(255, 205, 86)',
            ],
            barThickness: 40,
                   },
        ]
      },
      options: {
        maintainAspectRatio: false,
        indexAxis: 'y',
        scales: {
          y: {
            beginAtZero: false, 
            
          }
        }
      }
      
    });
  }
  
  printData(){
    console.log(this.dataBars2)
    
  }

  
  getCountries() {
  
    this.countries = this.dataBars2.slice(0, 10).map(covidData => covidData.country);
    return this.countries;
  }

   getCases() {
  
    this.totalCases = this.dataBars2.slice(0, 10).map(covidData => covidData.totalCases);
    return this.totalCases;
  }

  

}
