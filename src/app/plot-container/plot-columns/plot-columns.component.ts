import { Component, Input, SimpleChanges } from '@angular/core';
import { Covid, CovidModel } from 'src/app/covid.model';
import Chart from 'chart.js/auto';


@Component({
  selector: 'app-plot-columns',
  templateUrl: './plot-columns.component.html',
  styleUrls: ['./plot-columns.component.css']
})
export class PlotColumnsComponent {
  @Input() dataSector: Covid[] = [];
  public chart: any;

  private countries: string[] = [];
  private deaths: number[] = [];
  private recoveries: number[] = [];

  constructor() {
    this.countries = [];
    this.deaths = [];
    this.recoveries = [];
  }

  ngOnInit() {
    
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.dataSector.length > 0) this.createChart();
  }

  createChart() {
    this.countries = this.getCountries();
    this.deaths = this.getDeaths();
    this.recoveries = this.getRecoveries();
  
    if (this.chart) this.chart.destroy();
  
    this.chart = new Chart("Bars1Chart", {
      type: 'bar',
      data: {
        labels: this.countries,
        datasets: [
          {
            label: "Total Deaths",
            data: this.deaths, 
            backgroundColor: 'rgb(255, 99, 132)', 
            barThickness: 35
          },
          {
            label: "Total Recovered",
            data: this.recoveries,
            backgroundColor: 'rgb(75, 192, 192)', 
            barThickness: 35
          }
        ]
      },
      options: {
        maintainAspectRatio: false,
        scales: {
          y: {
            beginAtZero: false, 
            
          }
        }
      }
    });
  }
  
  printData(){
    console.log(this.dataSector)
    
  }

  
  getCountries() {
  
    this.countries = this.dataSector.slice(0, 10).map(covidData => covidData.country);
    return this.countries;
  }

  getDeaths() {
  
      this.deaths = this.dataSector.slice(0, 10).map(covidData => covidData.totalDeaths);
      console.log('Type:', typeof this.deaths);
      return this.deaths;

  }
  
  getRecoveries() {
  
    this.recoveries = this.dataSector.slice(0, 10).map(covidData => covidData.totalRecovered);
    return this.recoveries;
  }

  

}
