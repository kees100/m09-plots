import { Component, Input, SimpleChanges, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { Covid } from 'src/app/covid.model';
import Chart from 'chart.js/auto';

@Component({
  selector: 'app-plot-circular',
  templateUrl: './plot-circular.component.html',
  styleUrls: ['./plot-circular.component.css']
})
export class PlotCircularComponent {

  @Input() dataCircular: Covid[] = [];
  public chart: any;

  private countries: string[] = [];
  private activeCases: number[] = [];


  constructor() {
    this.countries = [];
    this.activeCases = [];
  }
  ngOnInit() {
    
  }

 
  ngOnChanges(changes: SimpleChanges) {
    if (this.dataCircular.length > 0) this.createDoughnutChart();
  }


  private createDoughnutChart() {
    const countries = this.getCountries();
    const activeCases = this.getActiveCases();

    if (this.chart) {
      this.chart.destroy();
    }

    this.chart = new Chart("doughnutCanva", {
      type: 'doughnut',
      data: {
        labels: this.countries,
        datasets: [
          {
            label: "Active cases",
            data: this.activeCases, 
            backgroundColor: [
              'rgb(255, 99, 132)',
              'rgb(255, 159, 64)',
              'rgb(255, 205, 86)',
              'rgb(75, 192, 192)',
              'rgb(54, 162, 235)',
              'rgb(153, 102, 255)',
              'rgb(201, 203, 207)',
              'rgb(255, 99, 132)',
              'rgb(255, 159, 64)',
              'rgb(255, 205, 86)',
            ], 
            hoverOffset: 4
          },
      
        ]
      },
      options: {
        maintainAspectRatio: false,
    
      }
    });

  }

  printData() {
    console.log(this.dataCircular);
  }

  getCountries() {
    this.countries = this.dataCircular.slice(0, 10).map(covidData => covidData.country);
    return this.countries;
  }

  getActiveCases() {
    this.activeCases = this.dataCircular.slice(0, 10).map(covidData => covidData.activeCases);
    return this.activeCases;
  }

}
