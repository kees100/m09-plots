import { Component, Input } from '@angular/core';
import { Covid } from 'src/app/covid.model';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  uploadFile($event: Event) {
    throw new Error('Method not implemented.');
  }
  title = 'plots';
  currentData: Covid[] = [];

  getData(data: Covid[]) {
    this.currentData = data;
  }
}
