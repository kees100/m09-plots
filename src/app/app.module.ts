import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { PlotContainerComponent } from './plot-container/plot-container.component';
import { UploadFileComponent } from './upload-file/upload-file.component';
import { PlotGeographComponent } from './plot-container/plot-geograph/plot-geograph.component';
import { PlotTableComponent } from './plot-container/plot-table/plot-table.component';
import { PlotColumnsComponent } from './plot-container/plot-columns/plot-columns.component';
import { PlotCircularComponent } from './plot-container/plot-circular/plot-circular.component';

@NgModule({
  declarations: [
    AppComponent,
    PlotContainerComponent,
    UploadFileComponent,
    PlotGeographComponent,
    PlotTableComponent,
    PlotColumnsComponent,
    PlotCircularComponent
  ],
  imports: [
    BrowserModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
