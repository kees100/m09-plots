import { Component } from '@angular/core';
import { Covid, CovidModel } from '../covid.model';
import { Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.css']
})
export class UploadFileComponent {
  mostrarBarraProgreso = false;
  progresoCarga = 0;
  file: any;
  fileContent: any;
  data: Covid[] = [];
  @Output() newItemEvent = new EventEmitter<Covid[]>();
 
  constructor() {}
 
  ngOnInit(): void {}

  sendDataToParent() {
    this.newItemEvent.emit(this.data);
  }
 
  uploadFile(event: any){
    const file = event.target.files[0];
    if (file) {
      const reader = new FileReader();
 
      reader.onload = (e) => {
        this.fileContent = e.target?.result;
        console.log('File Content:', this.fileContent);
        this.parseFile();
      };
 
      reader.readAsText(file); // Assuming you want to read the file as text, adjust as needed
    }
  }
 
 
 
  parseFile(){
    const rows = this.fileContent.split('\n');
    const headers = rows[0].split(',');
 
    for (let i = 1; i < rows.length; i++) {
      const values = rows[i].split(',');
      const covidData: Covid = new CovidModel();
 
      for (let j = 0; j < headers.length && rows.length > 1; j++) {
        const key = headers[j].trim() as keyof Covid;
        covidData[key] = values[j].trim() as never;
        // covidData[key] = this.convertToNumber(values[j].trim()) as never;
      }
      this.data.push(covidData);
    }
 
    this.sendDataToParent();
  }
 
  private convertToNumber(value: string): number {
    const numericValue = parseFloat(value);
    return isNaN(numericValue) ? 0 : numericValue;
  }
}



